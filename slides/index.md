# whoami

- Kolokotronis Panagiotis. Find me @ [panagiks.com](https://panagiks.com)
- Software Engineer @ noris M.I.K.E.
- Python, Security, Open Source

---

# Deployment testing with OKD
## Testing beyond CD

///

## Purposes of this workshop

Yes:

- Cover a not so frequently discussed area of testing
- (hopefully) Inspire people to further look into deployment testing

No:

- Provide an exaustive how-to

///

## Table of Contents

- Definitions
- Tools used
- An example app
- OKD lifecycle hooks
- Demo

---

# Definitions

///

## What is CI

_Continuous Integration_

Automates:

- Blending individual parts of code (maybe from different teams)
- Code Testing to find bugs as early as possible

///

## What is CD

_Continuous Deployment_ or _Continuous Delivery_

Automates:

- Keeping the latest working version always available
- Delivering multiple small changes vs Delivering one big block of multiple changes
- Deployment of the application on the various environments

---

# Tools

- gitlab-ci
- Openshift (OKD 3.11)

///

## gitlab-ci

- CI/CD platform by Gitlab
- Pipeline as Code => Versioned, auditable, centralized (platform) yet distributed (project)
- SaaS & Self-Hosted

///

## Openshift

Kubernetes on Steroids!

- Redhat product
- Open Source flavor is called OKD

---

# Our example App

- An [app](https://gitlab.com/panagiks/JobFestivalApp) from [a previous workshop](https://panagiks.gitlab.io/JobFestivalCICD) as base
- Simple python web server
- Has tests run during CI
- Has CD

///

## Quick intro to our example app

---

# Deployment testing

///

## Why

- Four-eyes principle for your tests
- An 'expensive' yet effective safety net for critical checks
- A good place to catch configuration issues early

///

## Exapmle cases

- Configuration sanity check
  - Does the new configuration render the app inoperable (i.e. typo in token signing key)
- Four-eyes principle
  - Did someone accidentally brake auth (and just commented out the tests)

///

## How

- Utilize OKD's [lifecycle hooks](https://docs.okd.io/3.11/dev_guide/deployments/deployment_strategies.html#pod-based-lifecycle-hook)
- If you need the app running go for `post` pod lifecycle hook
- Otherwise go for the `pre` pod lifecycle hook

///

## Our case

Let's assume that:

- We have very expensive initialization
- We can do some sanity check on our config
- If something looks wrong we shouldn't bother start the app & wait for it to fail

///


```yaml
....
      rollingParams:
        pre:
          failurePolicy: Abort
          execNewPod:
            containerName: jobfestivalapp
            command:
              - /bin/bash
              - '-c'
              - check_settings
....
```

---

# Demo

![](slides/images/demo_meme2.jpg)

---

# Questions ?

---

# Jobs

We are looking for:

- k8s / OKD operators
- Sr. Python developers
- Frontend (Angular) developers

jobs [at] noris.gr

---

# Get the slides

[https://panagiks.gitlab.io/deployment-testing/](https://panagiks.gitlab.io/deployment-testing/)
